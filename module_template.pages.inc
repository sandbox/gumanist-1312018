<?php
/**
 * @file
 * Working pages for module generation.
 */

/**
 * Page for generating module from predefined template.
 *
 * @param  $form
 * @param  $form_state
 * @return void
 */
function module_template_homepage_form($form, &$form_state) {
  $form = _module_template_form_default();

  $form['template'] = array(
    '#type' => 'select',
    '#title' => t('Select template'),
    '#description' => t('Select template type from the list'),
    '#default_value' => 'custom',
    '#options' => $form['defaults']['#templates_list'],
  );

  $form['module_defaults']['actions'] = array('#type' => 'actions');
  $form['actions']['download'] = array(
    '#type' => 'submit',
    '#value' => t('Download module'),
  );
  $form['module_defaults']['actions']['configure'] = array(
    '#markup' => l(t('Configure'), 'admin/config/development/module_template/configure'),
  );

  return $form;
}

/**
 * Predefined template generation submit.
 *
 * @param  $form
 * @param  $form_state
 * @return void
 */
function module_template_homepage_form_submit($form, &$form_state) {
  if ($form_state['values']['op'] == t('Configure')) {
    drupal_goto('admin/config/development/module_template/configure');
  }

  if ($form_state['values']['op'] == t('Download module')) {
    $template = $form['defaults']['#module_templates'][$form_state['input']['template']];

    if (array_key_exists('presets', $template) && is_array($template['presets'])) {
      $presets = module_template_module_template_presets();
      foreach ($template['presets'] as $preset) {
        if (array_key_exists($preset, $presets)) {
          $template['actions'] = array_merge($template['actions'], $presets[$preset]['actions']);
        }
      }
    }

    $settings['#actions'] = $template['actions'];
    foreach ($form_state['input'] as $key => $value) {
      if (!empty($value)) {
        $settings['#' . $key] = $value;
      }
    }

    module_template_generate_module($settings);
  }
}

/**
 * Module configuration form.
 *
 * @param  $form
 * @param  $form_state
 * @return void
 */
function module_template_configure_module_form($form, &$form_state) {
  $form = _module_template_form_default();

  $presets = module_template_module_template_presets();
  $form['defaults']['#presets'] = $presets;
  $form['settings'] = array(
    '#type' => 'container',
    '#attributes' => array(),
  );

  $form['settings']['description'] = array(
    '#markup' => t('Select necessary parts which you need in your module.'),
  );

  foreach ($presets as $key => $preset) {
    $form['settings'][$key] = array(
      '#type' => 'container',
      '#attributes' => array(
        'class' => array(
          'settings-item'
        )
      ),
      '#tree' => TRUE,
    );

    $form['settings'][$key]['selected'] = array(
      '#type' => 'checkbox',
      '#title' => $preset['title'],
      '#description' => $preset['description'],
      '#default_value' => FALSE,
      '#attributes' => array(
        'class' => array(
          'settings-item-status'
        )
      ),
    );

    if (array_key_exists('settings', $preset) && is_array($preset['settings'])) {
      foreach ($preset['settings'] as $name => $value) {
        switch ($value['#settings_type']) {
          case 'replacement': {
            $group_title = t('@preset_title replacement items', array('@preset_title' => $preset['title']));
            break;
          }
          case 'settings': {
            $group_title = t('@preset_title settings', array('@preset_title' => $preset['title']));
            break;
          }
          default: {
            $group_title = t('Unknown group.');
          }
        }

        if (!array_key_exists($value['#settings_type'], $form['settings'][$key])) {
          $form['settings'][$key][$value['#settings_type']] = array(
            '#type' => 'fieldset',
            '#title' => $group_title,
            '#attributes' => array(
              'class' => array(
                'display: none'
              )
            ),
          );
        }

        $form['settings'][$key][$value['#settings_type']][$name] = $value;
      }
    }
  }

  $form['module_defaults']['actions'] = array('#type' => 'actions');
  $form['module_defaults']['actions']['download'] = array(
    '#type' => 'submit',
    '#value' => t('Download module'),
  );

  return $form;
}

/**
 * Configured module generation submit handling.
 *
 * @param  $form
 * @param  $form_state
 * @return void
 */
function module_template_configure_module_form_submit($form, &$form_state) {
  $template['actions'] = array();
  $presets = $form['defaults']['#presets'];
  foreach ($presets as $key => $preset) {
    if ($form_state['values'][$key]['selected']) {
      $settings = array(
        '#replacement' => array_key_exists('replacement', $form_state['values'][$key]) ? $form_state['values'][$key]['replacement'] : array(),
        '#settings' => array_key_exists('settings', $form_state['values'][$key]) ? $form_state['values'][$key]['settings'] : array(),
      );
      $actions = array();
      foreach ($preset['actions'] as $key => $action) {
        $actions[$key] = _module_templates_add_settings($action, $settings);
      }
      $template['actions'] = array_merge($template['actions'], $actions);
    }
  }

  $settings['#actions'] = $template['actions'];
  foreach ($form_state['input'] as $key => $value) {
    if (!empty($value)) {
      $settings['#' . $key] = $value;
    }
  }

  module_template_generate_module($settings);
}

/**
 * Default module items generation
 *
 * @return array
 */
function _module_template_form_default() {
  $module_path = drupal_get_path('module', 'module_template');
  drupal_add_js($module_path . '/js/module-template.js');

  $form = array();

  $modules = module_implements('module_templates');
  $templates = array();
  $templates_list = array();
  foreach ($modules as $module) {
    $module_templates = module_invoke($module, 'module_templates');
    if (isset($module_templates) && is_array($module_templates)) {
      $templates = array_merge($templates, $module_templates);
      foreach ($module_templates as $name => $template) {
        $templates_list[$name] = $template['title'];
      }
    }
  }

  $form = array();
  $form['module_defaults'] = array(
    '#type' => 'container',
    '#attributes' => array()
  );
  $form['defaults']['#module_templates'] = $templates;
  $form['defaults']['#templates_list'] = $templates_list;

  $form['module_defaults']['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#description' => t(''),
    '#size' => 35,
  );

  $form['module_defaults']['module_name'] = array(
    '#type' => 'machine_name',
    '#machine_name' => array(
      'source' => array('module_defaults', 'name'),
      'exists' => '_module_template_exist',
    ),
    '#title' => t('Machine-readable name'),
    '#description' => t('Machine-readable name. May only contain lowercase letters, numbers and underscores.'),
    '#size' => 35,
  );

  $form['module_defaults']['description'] = array(
    '#type' => 'textfield',
    '#title' => t('Description'),
    '#size' => 35,
    '#description' => t('')
  );

  $form['module_defaults']['package'] = array(
    '#type' => 'textfield',
    '#title' => t('Package'),
    '#size' => 35,
    '#description' => t('')
  );

  $form['module_defaults']['version'] = array(
    '#type' => 'textfield',
    '#size' => 35,
    '#title' => t('Version'),
    '#description' => t(''),
    '#default_value' => '7.x-dev',
  );

  $form['module_defaults']['core'] = array(
    '#type' => 'textfield',
    '#size' => 35,
    '#title' => t('Core version'),
    '#description' => t(''),
    '#default_value' => '7.x',
  );

  return $form;
}

/**
 * Dummy function. We don't need to check module name existent at this moment.
 *
 * @return bool
 */
function _module_template_exist() {
  return FALSE;
}



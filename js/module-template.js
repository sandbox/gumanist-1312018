(function ($) {

Drupal.behaviors.moduleTemplate = {
  attach: function(context) {

    // Toggles settings subform visibility.
    $selector = '.settings-item-status';
    $($selector, context)
      .once('module-template', function() {
        if ($(this).val()) {
          $(this).parents('.settings-item').children('.form-wrapper').hide();
        } else {
          $(this).parents('.settings-item').children('.form-wrapper').show();
        }
      })
      .change(function() {
        $(this).parents('.settings-item').children('.form-wrapper').toggle();
      });
  }
}

})(jQuery);

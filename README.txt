This module creates an empty module with selected features. It generates necessary files and functions which will be used by developers to develop their great modules.

An idea of this project is to minify initial steps while creating custom module by providing correct naming and possibility to add code snippets by selecting necessary features. It should cover next tasks:
- quick module creation
- help with correct naming and parameters for the functions
- implement code templates for other modules

Its place between examples and features modules.

At this moment realized the only three options:
- empty form
- hook_menu
- views
- page

Future development will include drush integration and number of other options (blocks creation, ctools modal form, etc).

Any suggestions are accepted and welcomed in issues.